package com.senac.ads.experts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.senac.ads.experts.model.Convidados;
import com.senac.ads.experts.service.ConvidadoService;

@Controller
public class ConvidadoController {

	@Autowired
	private ConvidadoService service;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String index() {

		return "index";
	}

	@RequestMapping("listaTodosConvidados")
	public String listaTodosConvidados(Model model) {
		Iterable<Convidados> convidados = service.listaTodosConvidados();
		model.addAttribute("convidados", convidados);
		return "listaTodosConvidados";

	}
	
	@RequestMapping(value="salvar", method = RequestMethod.POST)
	public String salvar (@RequestParam("nome") String nome, 
						  @RequestParam("email") String email,
						  @RequestParam("telefone") String telefone, 
						  Model model) {
		
		Convidados novoConvidado = new Convidados(nome, email, telefone);
		service.salvar(novoConvidado);
		
		Iterable<Convidados> convidados = service.listaTodosConvidados();
		model.addAttribute("convidados", convidados);
		
		return "listaTodosConvidados";
			
	}

}
