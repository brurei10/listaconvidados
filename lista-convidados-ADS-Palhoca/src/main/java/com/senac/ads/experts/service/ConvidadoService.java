package com.senac.ads.experts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.senac.ads.experts.model.Convidados;
import com.senac.ads.experts.repository.ConvidadosRepository;

@Service
public class ConvidadoService {
	
	@Autowired
	private ConvidadosRepository repositorio;
	
	public Iterable<Convidados> listaTodosConvidados(){
		
		return repositorio.findAll();	
	}
	
	public void salvar(Convidados conv){
		
		repositorio.save(conv);
	}
}
