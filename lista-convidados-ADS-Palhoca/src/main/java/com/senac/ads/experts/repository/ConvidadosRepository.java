package com.senac.ads.experts.repository;

import org.springframework.data.repository.CrudRepository;

import com.senac.ads.experts.model.Convidados;

public interface ConvidadosRepository extends CrudRepository<Convidados, Long> {
	
}

