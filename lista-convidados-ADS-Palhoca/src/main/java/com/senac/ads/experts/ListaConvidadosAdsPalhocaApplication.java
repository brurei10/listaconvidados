package com.senac.ads.experts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ListaConvidadosAdsPalhocaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListaConvidadosAdsPalhocaApplication.class, args);
	}

}
